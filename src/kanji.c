#include <stdio.h>
#include "kanji.h"
#include "utils.h"

void PrintKanji(int n, int digit)
{
    if (n == 0 || digit < 1 || digit > DIGITS_MAX)
        return;
    else
        switch (n) {
            case 1:
                if (digit == 1)
                    printf("一");
                else if (digit == 2)
                    printf("十");
                else if (digit == 3)
                    printf("百");
                else if (digit == 4)
                    printf("千");
                else if (digit == 5)
                    printf("一万");
                break;
            case 2:
                if (digit == 1)
                    printf("二");
                else if (digit == 2)
                    printf("二十");
                else if (digit == 3)
                    printf("二百");
                else if (digit == 4)
                    printf("二千");
                else if (digit == 5)
                    printf("二万");
                break;
            case 3:
                if (digit == 1)
                    printf("三");
                else if (digit == 2)
                    printf("三十");
                else if (digit == 3)
                    printf("三百");
                else if (digit == 4)
                    printf("三千");
                else if (digit == 5)
                    printf("三万");
                break;
            case 4:
                if (digit == 1)
                    printf("四");
                else if (digit == 2)
                    printf("四十");
                else if (digit == 3)
                    printf("四百");
                else if (digit == 4)
                    printf("四千");
                else if (digit == 5)
                    printf("四万");
                
                break;
            case 5:
                if (digit == 1)
                    printf("五");
                else if (digit == 2)
                    printf("五十");
                else if (digit == 3)
                    printf("五百");
                else if (digit == 4)
                    printf("五千");
                else if (digit == 5)
                    printf("五万");
                break;
            case 6:
                if (digit == 1)
                    printf("六");
                else if (digit == 2)
                    printf("六十");
                else if (digit == 3)
                    printf("六百");
                else if (digit == 4)
                    printf("六千");
                else if (digit == 5)
                    printf("六万");
                break;
            case 7:
                if (digit == 1)
                    printf("七");
                else if (digit == 2)
                    printf("七十");
                else if (digit == 3)
                    printf("七百");
                else if (digit == 4)
                    printf("七千");
                else if (digit == 5)
                    printf("七万");
                break;
            case 8:
                if (digit == 1)
                    printf("八");
                else if (digit == 2)
                    printf("八十");
                else if (digit == 3)
                    printf("八百");
                else if (digit == 4)
                    printf("八千");
                else if (digit == 5)
                    printf("八万");
                break;
            case 9:
                if (digit == 1)
                    printf("九");
                else if (digit == 2)
                    printf("九十");
                else if (digit == 3)
                    printf("九百");
                else if (digit == 4)
                    printf("九千");
                else if (digit == 5)
                    printf("九万");
                break;
            default:
                break;
        }
}