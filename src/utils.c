#include "utils.h"
#include <stdlib.h>
#include <time.h>

int GetDigitCount(int const n)
{
    int digitCount = 1;
    int tempN = n;

    while (tempN > 9) {
        tempN /= 10;
        digitCount++;
    }

    return digitCount;
}

int GetRandNumber(int const digits)
{
    int randN, upperLimit = 1;

    for (int i = 0; i < digits; i++) {
        upperLimit *= 10;
    }
    
    srand(time(0));
    randN = rand() % (upperLimit);
    if (randN == 0) randN = 1;

    return randN;
}