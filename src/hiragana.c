#include <stdio.h>
#include "hiragana.h"
#include "utils.h"

void PrintHiragana(int n, int digit)
{
    if (n == 0 || digit < 1 || digit > DIGITS_MAX)
        return;
    else
        switch (n) {
            case 1:
                if (digit == 1)
                    printf("いち");
                else if (digit == 2)
                    printf("じゅう");
                else if (digit == 3)
                    printf("ひゃく");
                else if (digit == 4)
                    printf("せん");
                else if (digit == 5)
                    printf("いちまん");
                break;
            case 2:
                if (digit == 1)
                    printf("に");
                else if (digit == 2)
                    printf("にじゅう");
                else if (digit == 3)
                    printf("にひゃく");
                else if (digit == 4)
                    printf("にせん");
                else if (digit == 5)
                    printf("にまん");
                break;
            case 3:
                if (digit == 1)
                    printf("さん");
                else if (digit == 2)
                    printf("さんじゅう");
                else if (digit == 3)
                    printf("さんびゃく");
                else if (digit == 4)
                    printf("さんぜん");
                else if (digit == 5)
                    printf("さんまん");
                break;
            case 4:
                if (digit == 1)
                    printf("よん");
                else if (digit == 2)
                    printf("よんじゅう");
                else if (digit == 3)
                    printf("よんひゃく");
                else if (digit == 4)
                    printf("よんせん");
                else if (digit == 5)
                    printf("よんまん");
                
                break;
            case 5:
                if (digit == 1)
                    printf("ご");
                else if (digit == 2)
                    printf("ごじゅう");
                else if (digit == 3)
                    printf("ごひゃく");
                else if (digit == 4)
                    printf("ごせん");
                else if (digit == 5)
                    printf("ごまん");
                break;
            case 6:
                if (digit == 1)
                    printf("ろく");
                else if (digit == 2)
                    printf("ろくじゅう");
                else if (digit == 3)
                    printf("ろっぴゃく");
                else if (digit == 4)
                    printf("ろくせん");
                else if (digit == 5)
                    printf("ろっまん");
                break;
            case 7:
                if (digit == 1)
                    printf("なな");
                else if (digit == 2)
                    printf("ななじゅう");
                else if (digit == 3)
                    printf("ななひゃく");
                else if (digit == 4)
                    printf("ななせん");
                else if (digit == 5)
                    printf("ななまん");
                break;
            case 8:
                if (digit == 1)
                    printf("はち");
                else if (digit == 2)
                    printf("はちじゅう");
                else if (digit == 3)
                    printf("はっぴゃく");
                else if (digit == 4)
                    printf("はっせん");
                else if (digit == 5)
                    printf("はちまん");
                break;
            case 9:
                if (digit == 1)
                    printf("きゅう");
                else if (digit == 2)
                    printf("きゅうじゅう");
                else if (digit == 3)
                    printf("きゅうひゃく");
                else if (digit == 4)
                    printf("きゅうせん");
                else if (digit == 5)
                    printf("きゅうまん");
                break;
            default:
                break;
        }
}