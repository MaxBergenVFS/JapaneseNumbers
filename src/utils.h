#ifndef utils_h
#define utils_h

#define DIGITS_MAX 5

int GetDigitCount(int n);

int GetRandNumber(int const digits);

#endif