#include <stdio.h>
#include "hiragana.h"
#include "kanji.h"
#include "utils.h"

int main(void)
{
    int n, digitCount, userInput, correctAnswer, userAnswer, 
        userCorrectCount = 0, difficultyLevel = 1, levelProgressionThreshold = 4;

    printf("--------WELCOME TO JAPANESE NUMBER TESTER--------\n");
    printf("Press 1 for hiragana. Press 2 for kanji. Press 0 to quit: ");
    scanf("%d", &userInput);
    printf("\n");

    while (userInput > 0) {
        n = GetRandNumber(difficultyLevel);
        correctAnswer = n;
        digitCount = GetDigitCount(n);

        if (userInput < 3) {
            while (n > 0) {
                int multipleOfTen = 1, nextDigit;

                for (int i = 0; i < (digitCount - 1); i++)
                    multipleOfTen *= 10;

                nextDigit = n / multipleOfTen;

                if (userInput == 1)
                    PrintHiragana(nextDigit, digitCount);
                else if (userInput == 2)
                    PrintKanji(nextDigit, digitCount);
                    
                n = n - (nextDigit * multipleOfTen);
                digitCount = GetDigitCount(n);
            }
        } else {
            printf("Please enter a valid input\n\n");
            userInput = 0;
            goto prompt_user;
        }

        printf("\n\n");
        printf("Enter the value of this number: ");
        scanf("%d", &userAnswer);

        //check user answer
        if (userAnswer == correctAnswer) {
            printf("Correct!\n");
            userCorrectCount++;
        } else {
            printf("Wrong. The correct answer is %d\n", correctAnswer);
        }

        //increase difficulty
        if (userCorrectCount % levelProgressionThreshold == 0 && difficultyLevel < DIGITS_MAX)
            difficultyLevel++;

        prompt_user:
        printf("Press 1 for hiragana. Press 2 for kanji. Press 0 to quit: ");
        scanf("%d", &userInput);
        printf("\n");
    }
}